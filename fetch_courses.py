#!/bin/python

# Copyright (C) 2017  Marco Scarpetta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from urllib import request, parse
from html.parser import HTMLParser
import os
import json

current_year = 2017

courses_list_url = "https://poliba.esse3.cineca.it/ListaCorsiDiStudio.do"
course_url = "https://poliba.esse3.cineca.it/CorsoDiStudio.do"
pds_url = "https://poliba.esse3.cineca.it/OffertaDidatticaPDSORD.do"

# pds_id = 9999 insegnamenti comuni
# pds_id > 9999 curricula

def gen_data(data):
    return bytes(parse.urlencode(data), encoding="utf-8")

class CoursesListExtractor(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.data_is_course_name = False
        self.data_is_course_duration = False
        self.courses = []
    
    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for (attr, value) in attrs:
                if attr == "href" and value.startswith("CorsoDiStudio.do"):
                    self.data_is_course_name = True
                    self.courses.append({"cds_id": value.split("cds_id=")[1]})

    def handle_data(self, data):
        if self.data_is_course_duration:
            self.courses[-1]["duration"] = int(data)
            self.data_is_course_duration = False
        if self.data_is_course_name:
            self.courses[-1]["name"] = data.replace("\\", "")
            self.data_is_course_name = False
            self.data_is_course_duration = True

class pds_ids_Extractor(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.data_is_pds_name = False
        self.pds_ids = []
    
    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for (attr, value) in attrs:
                if attr == "href" and value.startswith("OffertaDidatticaPDSORD.do"):
                    self.pds_ids.append({"id": value.split("pds_id=")[1]})
                    self.data_is_pds_name = True

    def handle_data(self, data):
        if self.data_is_pds_name:
            self.pds_ids[-1]["name"] = data.replace("\\", "")
            self.data_is_pds_name = False

class aa_ord_ids_Extractor(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.aa_ord_ids = []
    
    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for (attr, value) in attrs:
                if attr == "href" and value.startswith("CorsoDiStudio.do"):
                    self.aa_ord_ids.append(int(value.split("aa_ord_id=")[1]))

class ExamsExtractor(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.data_is_exam_name = False
        self.data_is_exam_cfu = False
        self.cfu_sum = 0
        self.exams = []
    
    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for (attr, value) in attrs:
                if attr == "href" and value.startswith("AttivitaDidatticaContestualizzata.do"):
                    self.data_is_exam_name = True
                    self.exams.append({})

    def handle_data(self, data):
        if self.data_is_exam_cfu:
            self.exams[-1]["cfu"] = int(data)
            self.cfu_sum += int(data)
            self.data_is_exam_cfu = False
        if self.data_is_exam_name:
            self.exams[-1]["name"] = data.replace("\\", "")
            self.data_is_exam_name = False
            self.data_is_exam_cfu = True

# fetch courses list
print('[{: >3}%] Fetching courses list'.format(0))
f = request.urlopen(courses_list_url)
parser = CoursesListExtractor()
parser.feed(str(f.read()))

# save courses list
f = open("data/courses.json", "w")
json.dump(parser.courses, f, indent=4)

progress = 1
total = len(parser.courses)+1

print('[{: >3}%] DONE'.format(int(progress/total*100)))

for i in range(len(parser.courses)):
    course = parser.courses[i]
    course_data = {}
    
    print('[{: >3}%] Fetching course "{}"'.format(int(progress/total*100), course["name"]))
    
    # fetch all aa_ord_ids of this course
    aa_ord_ids = []
    f = request.urlopen(course_url, gen_data({"cds_id": course["cds_id"]}))
    aa_ord_ids_parser = aa_ord_ids_Extractor()
    aa_ord_ids_parser.feed(str(f.read()))
    
    if (len(aa_ord_ids_parser.aa_ord_ids) > 0):
        aa_ord_ids.append(aa_ord_ids_parser.aa_ord_ids[0])
        f = request.urlopen(course_url, gen_data({"cds_id": course["cds_id"], "aa_ord_id": aa_ord_ids[0]}))
    else:
        f = request.urlopen(course_url, gen_data({"cds_id": course["cds_id"], "aa_ord_id": 1990}))
    aa_ord_ids_parser = aa_ord_ids_Extractor()
    aa_ord_ids_parser.feed(str(f.read()))
    
    for aa_ord_id in aa_ord_ids_parser.aa_ord_ids:
        aa_ord_ids.append(aa_ord_id)
    
    for j in range(len(aa_ord_ids)):
        #fetch pds_ids for each aa_ord_id
        f = request.urlopen(course_url, gen_data({"cds_id": course["cds_id"], "aa_ord_id": aa_ord_ids[j]}))
        pds_id_parser = pds_ids_Extractor()
        pds_id_parser.feed(str(f.read()))
        
        year = aa_ord_ids[j]
        stop = aa_ord_ids[j+1] if j+1 < len(aa_ord_ids) else current_year
        
        while year <= stop:
            course_data[year] = {}
            for pds_id in pds_id_parser.pds_ids:
                f = request.urlopen(pds_url, gen_data({
                    "cds_id": course["cds_id"],
                    "aa_ord_id": aa_ord_ids[j],
                    "pds_id": pds_id["id"],
                    "aa_off_id": year}))
                
                # fetch exams list for this year and pds_id
                exams_extractor = ExamsExtractor()
                try:
                    exams_extractor.feed(str(f.read()))
                except:
                    print('[{: >3}%] ERROR exams list errors at url: {}'.format(
                        int(progress/total*100),
                        pds_url + "?" + gen_data({
                            "cds_id": course["cds_id"],
                            "aa_ord_id": aa_ord_ids[j],
                            "pds_id": pds_id["id"],
                            "aa_off_id": year}).decode("utf-8")))
                
                # try to fix missing cfus
                initial_cfu_sum = exams_extractor.cfu_sum
                if course["duration"]*60 - exams_extractor.cfu_sum >= 12:
                    exams_extractor.exams.append({"name": "ESAME A SCELTA", "cfu": 6})
                    exams_extractor.exams.append({"name": "ESAME A SCELTA", "cfu": 6})
                    exams_extractor.cfu_sum += 12
                if course["duration"]*60 - exams_extractor.cfu_sum >= 6:
                    exams_extractor.exams.append({"name": "ESAME A SCELTA", "cfu": 6})
                    exams_extractor.cfu_sum += 6
                if course["duration"]*60 - exams_extractor.cfu_sum == 3:
                    exams_extractor.exams.append({"name": "ESAME A SCELTA", "cfu": 3})
                    exams_extractor.cfu_sum += 3
                
                # print an error if cfu sum is wrong
                if exams_extractor.cfu_sum != course["duration"]*60:
                    print('[{: >3}%] ERROR wrong cfu sum [{: >3}/{: >3}] {} {}'.format(
                        int(progress/total*100),
                        initial_cfu_sum,
                        course["duration"]*60,
                        year,
                        pds_id["name"]))
                # add exams list to course_data if cfu sum is correct
                else:
                    course_data[year][pds_id["name"]] = exams_extractor.exams

            year += 1
    
    # save course informations
    f = open("data/{}.json".format(course["cds_id"]), "w")
    json.dump(course_data, f, indent=4)
    
    progress += 1
    print('[{: >3}%] DONE'.format(int(progress/total*100)))
