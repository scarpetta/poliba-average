/*
 *	@licstart  The following is the entire license notice for the
 *    JavaScript code in this page.
 *
 *    Copyright (C) 2014-2017  Marco Scarpetta
 *
 *    The JavaScript code in this page is free software: you can
 *    redistribute it and/or modify it under the terms of the GNU
 *    General Public License (GNU GPL) as published by the Free Software
 *    Foundation, either version 3 of the License, or (at your option)
 *    any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *    without even the implied warranty of MERCHANTABILITY or FITNESS
 *    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *    As additional permission under GNU GPL version 3 section 7, you
 *    may distribute non-source (e.g., minimized or compacted) forms of
 *    that code without the copy of the GNU GPL normally required by
 *    section 4, provided you include this license notice and a URL
 *    through which recipients can access the Corresponding Source.
 *
 *    @licend  The above is the entire license notice
 *    for the JavaScript code in this page.
 */

var career_id = null;
var career = null;
var career_is_new = false;
var cds_id = null;
var cds_name = null;
var cds_duration = null;
var year = null;
var curriculum = null;
var cds_data = null;

var course_select_view = null;
var year_select_view = null;
var curriculum_select_view = null;
var home_view = null;

function update_average() {
    var totCredits = 0;
    var sum = 0;
    for (var i=0; i<career.exams.length; i++) {
        var credits = career.exams[i].credits;
        var mark = career.exams[i].mark;
        if (mark < 31 && mark > 17) {
            totCredits = totCredits + credits;
            sum = sum + mark*credits;
        }
    }
    var remCredits = career.removeLowestCredits;
    var removed = new Array();
    while (remCredits > 0) {
        lowest = null;
        for (var i=0; i<career.exams.length; i++) {
            if ((removed.indexOf(i) < 0) && (career.exams[i].mark < 31) && (career.exams[i].mark > 17)) {
                if (lowest !== null) {
                    if (career.exams[i].mark < career.exams[lowest].mark) {
                        lowest = i;
                    }
                }
                else {
                    lowest = i;
                }
            }
        }

        if (lowest !== null) {
            removed.push(lowest);
            if (career.exams[lowest].credits < remCredits) {
                sum  = sum - career.exams[lowest].mark*career.exams[lowest].credits;
                totCredits = totCredits - career.exams[lowest].credits;
                remCredits = remCredits - career.exams[lowest].credits;
            }
            else {
                sum  = sum - career.exams[lowest].mark*remCredits;
                totCredits = totCredits - remCredits;
                remCredits = 0;
            }
        }
        else {
            remCredits = 0;
        }
    }

    document.getElementById("wAverage").textContent = sum/totCredits;
}

function career_view()
{
    if (career == null)
        return home_view();

    var content_div = document.querySelector("#js_content");

    content_div.textContent = "";

    var career_name = document.createElement("input");
    career_name.type = "text";
    career_name.value = career.name;
    career_name.size = 50;
    career_name.id = "career_name";
    content_div.appendChild(career_name);

    var exams_table = document.createElement("table");
    content_div.appendChild(exams_table);

    for (var i=0; i<career.exams.length; i++) {
        var tr = document.createElement("tr");
        exams_table.appendChild(tr);

        var name = document.createElement("td");
        name.appendChild(document.createTextNode(career.exams[i].name));
        var credits = document.createElement("td");
        credits.appendChild(document.createTextNode(career.exams[i].credits));
        var mark = document.createElement("td");
        var input = document.createElement("input");
        input.type = "number";
        input.step = "1";
        input.size = 2;
        input.id = i;
        input.value = career.exams[i].mark;
        input.style.maxWidth = "2rem";
        input.addEventListener("input", function(evt) {
            career.exams[evt.target.id].mark = parseInt(evt.target.value);
            update_average();
        });

        mark.appendChild(input);
        tr.appendChild(name);
        tr.appendChild(credits);
        tr.appendChild(mark);
    }

    var h2 = document.createElement("h2");
    h2.appendChild(document.createTextNode("Media pesata: "));
    content_div.appendChild(h2);

    var span = document.createElement("span");
    span.id = "wAverage";
    h2.appendChild(span);

    update_average();

    var p = document.createElement("p");
    content_div.appendChild(p);

    var save_button = document.createElement("button");
    save_button.appendChild(document.createTextNode("Salva"));
    save_button.addEventListener("click", function() {
        career.name = career_name.value;
        localStorage.setItem(career_id, JSON.stringify(career));
    });
    p.appendChild(save_button);

    p = document.createElement("p");
    content_div.appendChild(p);

    var a = document.createElement("a");
    a.href = "javascript:void(0);";
    a.appendChild(document.createTextNode("Indietro"));
    a.addEventListener("click", function() {
        if (career_is_new)
            curriculum_select_view();
        else
            home_view();
    });
    p.appendChild(a);
}

course_select_view = function() {
    var content_div = document.querySelector("#js_content");

    content_div.textContent = "";

    function on_json_loaded () {
        var courses = JSON.parse(this.responseText);

        var table = document.createElement("table");
        content_div.appendChild(table);

        for (var i=0; i<courses.length; i++) {
            var tr = document.createElement("tr");
            table.appendChild(tr);

            var td = document.createElement("td");
            tr.appendChild(td);

            var a = document.createElement("a");
            a.href = "javascript:void(0);";
            a.dataset.cds_id = courses[i]["cds_id"];
            a.dataset.cds_name = courses[i]["name"];
            a.dataset.cds_duration = courses[i]["duration"];
            a.appendChild(document.createTextNode(courses[i]["name"]));
            a.addEventListener("click", function(evt) {
                cds_id = evt.target.dataset.cds_id;
                cds_name = evt.target.dataset.cds_name;
                cds_duration = evt.target.dataset.cds_duration;
                year_select_view();
            });
            td.appendChild(a)

            var td = document.createElement("td");
            tr.appendChild(td);

            var span = document.createElement("span");
            span.appendChild(document.createTextNode("Durata: " + courses[i]["duration"] + " anni"));
            td.appendChild(span);
        }

        var p = document.createElement("p");
        content_div.appendChild(p);

        var a = document.createElement("a");
        a.href = "javascript:void(0);";
        a.appendChild(document.createTextNode("Indietro"));
        a.addEventListener("click", home_view);
        p.appendChild(a);
    }

    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", on_json_loaded);
    oReq.open("GET", "data/courses.json");
    oReq.send();
}

year_select_view = function() {
    var content_div = document.querySelector("#js_content");

    content_div.textContent = "";

    function on_json_loaded () {
        cds_data = JSON.parse(this.responseText);

        var table = document.createElement("table");
        content_div.appendChild(table);

        for (var key in cds_data) {
            var tr = document.createElement("tr");
            table.appendChild(tr);

            var td = document.createElement("td");
            tr.appendChild(td);

            var a = document.createElement("a");
            a.href = "javascript:void(0);";
            a.dataset.year = key;
            a.appendChild(document.createTextNode("Anno Accademico " + key + "/" + (parseInt(key)+1)));
            a.addEventListener("click", function(evt) {
                year = evt.target.dataset.year;
                curriculum_select_view();
            });
            td.appendChild(a)
        }

        var p = document.createElement("p");
        content_div.appendChild(p);

        var a = document.createElement("a");
        a.href = "javascript:void(0);";
        a.appendChild(document.createTextNode("Indietro"));
        a.addEventListener("click", course_select_view);
        p.appendChild(a);
    }

    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", on_json_loaded);
    oReq.open("GET", "data/" + cds_id + ".json");
    oReq.send();
}

curriculum_select_view = function() {
    var content_div = document.querySelector("#js_content");

    content_div.textContent = "";

    var table = document.createElement("table");
    content_div.appendChild(table);

    for (var key in cds_data[year]) {
        var tr = document.createElement("tr");
        table.appendChild(tr);

        var td = document.createElement("td");
        tr.appendChild(td);

        var a = document.createElement("a");
        a.href = "javascript:void(0);";
        a.dataset.curriculum = key;
        a.appendChild(document.createTextNode(key));
        a.addEventListener("click", function(evt) {
            curriculum = evt.target.dataset.curriculum;
            career = {};

            career.name = cds_name + ' ' + (new Date()).toLocaleDateString();

            career.removeLowestCredits = 0;
            if (cds_duration == 3)
                career.removeLowestCredits = 12;
            else if (cds_duration == 2)
                career.removeLowestCredits = 9;

            career.exams = [];
            for (var i=0; i < cds_data[year][curriculum].length; i++) {
                var exam = {};
                exam.name = cds_data[year][curriculum][i]["name"];
                exam.credits = cds_data[year][curriculum][i]["cfu"];
                exam.mark = 0;
                career.exams.push(exam);
            }

            career_id = "career" + (new Date()).toISOString();
            career_is_new = true;
            career_view();
        });
        td.appendChild(a)
    }

    var p = document.createElement("p");
    content_div.appendChild(p);

    var a = document.createElement("a");
    a.href = "javascript:void(0);";
    a.appendChild(document.createTextNode("Indietro"));
    a.addEventListener("click", year_select_view);
    p.appendChild(a);
}

home_view = function()
{
    var content_div = document.querySelector("#js_content");

    content_div.textContent = "";

    var h2_title = document.createElement("h2");
    h2_title.appendChild(document.createTextNode("Carriere salvate"));
    content_div.appendChild(h2_title);

    var table = document.createElement("table");
    content_div.appendChild(table);

    for (var key in localStorage) {
        if (key.startsWith("career")) {
            var tr = document.createElement("tr");
            table.appendChild(tr);

            var td = document.createElement("td");
            tr.appendChild(td);

            var a = document.createElement("a");
            a.href = "javascript:void(0);";
            a.dataset.career_id = key;
            a.appendChild(document.createTextNode(JSON.parse(localStorage.getItem(key)).name));
            a.addEventListener("click", function(evt) {
                career_id = evt.target.dataset.career_id;
                career = JSON.parse(localStorage.getItem(career_id));
                career_is_new = false;
                career_view();
            });
            td.appendChild(a)

            var td = document.createElement("td");
            tr.appendChild(td);

            var a = document.createElement("a");
            a.href = "javascript:void(0);";
            a.dataset.career_id = key;
            a.appendChild(document.createTextNode("Cancella"));
            a.addEventListener("click", function(evt) {
                localStorage.removeItem(evt.target.dataset.career_id);
                home_view();
            });
            td.appendChild(a);
        }
    }

    var p = document.createElement("p");
    content_div.appendChild(p);

    var a = document.createElement("a");
    a.href = "javascript:void(0);";
    a.appendChild(document.createTextNode("Nuova Carriera..."));
    a.addEventListener("click", course_select_view);
    p.appendChild(a);
}

window.addEventListener("load", home_view);
